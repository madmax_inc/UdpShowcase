#include "dispatcher.hpp"

#include <sys/select.h>
#include <sys/socket.h>

namespace udpshowcase {
namespace core {

namespace {

class SocketDescriptorSet {
public:
  explicit SocketDescriptorSet() { FD_ZERO(&impl_); }

  void Set(const SocketHolder &holder) {
    const auto descriptor = holder.GetSocketDescriptor();
    FD_SET(descriptor, &impl_);

    max_value_ = std::max(descriptor, max_value_);
    size_++;
  }

  bool Contains(const SocketHolder &holder) const {
    return FD_ISSET(holder.GetSocketDescriptor(), &impl_);
  }

  std::size_t Size() const { return size_; }

  SocketDescriptor GetMax() const { return max_value_; }

  fd_set *operator->() { return &impl_; }

private:
  fd_set impl_;
  SocketDescriptor max_value_{};
  std::size_t size_{};
};

} // namespace

void Dispatcher::NotifyReadyRead(const SocketHolder &socket,
                                 GeneralCallback read_callback,
                                 const std::chrono::milliseconds &timeout) {
  read_queue_.DefferedPush({socket, read_callback, timeout});
}

void Dispatcher::NotifyReadyWrite(const SocketHolder &socket,
                                  GeneralCallback write_callback,
                                  const std::chrono::milliseconds &timeout) {
  write_queue_.DefferedPush({socket, write_callback, timeout});
}

void Dispatcher::Run() {
  while (true) {
    Dispatch();
  }
}

void Dispatcher::DispatchQueues() {
  read_queue_.Dispatch();
  write_queue_.Dispatch();
}

void Dispatcher::Dispatch(const std::chrono::milliseconds &timeout) {
  DispatchQueues();
  SocketDescriptorSet read_set;
  for (const auto &read_item : read_queue_.Impl()) {
    read_set.Set(read_item.holder);
  }

  SocketDescriptorSet write_set;
  for (const auto &write_item : write_queue_.Impl()) {
    write_set.Set(write_item.holder);
  }

  auto set_ptr = [](SocketDescriptorSet &set) -> fd_set * {
    if (!set.Size()) {
      return nullptr;
    } else {
      return set.operator->();
    }
  };

  const auto max_fd = std::max(read_set.GetMax(), write_set.GetMax()) + 1;
  timeval timeout_val{};
  if (timeout != timeout::kNoTimeout) {
    timeout_val = {
        /*.tv_sec = */ 0,
        /*.tv_usec = */ std::chrono::duration_cast<std::chrono::microseconds>(
            timeout)
            .count()};
  }

  timeval *timeout_val_ptr{};
  if (timeout != timeout::kNoTimeout) {
    timeout_val_ptr = &timeout_val;
  }

  const auto start_time = std::chrono::system_clock::now();

  auto result = select(max_fd, set_ptr(read_set), set_ptr(write_set), nullptr,
                       timeout_val_ptr);

  const auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(
      std::chrono::system_clock::now() - start_time);

  // Don't return immediately, check timeouts instead

  if (result == -1) {
    if (errno == EINTR) {
      return;
    }
  }

  auto queue_actor = [&result,
                      &elapsed](common::DefferedVector<DefferedInfo> &queue,
                                const SocketDescriptorSet &set,
                                const std::string &timeout_description) {
    for (auto it = queue.Impl().rbegin(); it != queue.Impl().rend(); ++it) {
      if (result && set.Contains(it->holder)) {
        it->callback({});

        queue.Impl().erase(--it.base());
        result--;
      } else if (it->timeout != timeout::kNoTimeout) {
        auto &item_timeout = it->timeout;

        if (elapsed > item_timeout) {
          it->callback(std::make_exception_ptr(
              exception::SocketTimeoutExpiredException{timeout_description}));
          queue.Impl().erase(--it.base());
        }

        item_timeout -= elapsed;
        if (item_timeout.count() < 0) {
        }
      }
    }
  };

  queue_actor(read_queue_, read_set, "while reading");
  queue_actor(write_queue_, write_set, "while writing");
}

} // namespace core
} // namespace udpshowcase
