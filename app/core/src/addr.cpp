#include "addr.hpp"

#include <sstream>

#include <arpa/inet.h>
#include <netdb.h>

#include "socket/endpoint_utils.hpp"

namespace udpshowcase {
namespace core {
namespace addr {

namespace {

Addr ParseAddr(const addrinfo &addr_info) {
  switch (addr_info.ai_family) {
  case AF_INET: {
    const auto *addr = reinterpret_cast<const sockaddr_in *>(addr_info.ai_addr);
    return FromSockAddr(*addr).addr;
  }
  case AF_INET6: {
    const auto *addr =
        reinterpret_cast<const sockaddr_in6 *>(addr_info.ai_addr);
    return FromSockAddr(*addr).addr;
  }
  default:
    throw std::runtime_error{"Bad addr type (" +
                             std::to_string(addr_info.ai_family) + ")"};
  }
}

} // namespace

template <> std::string ToString(const Ipv4 &addr) {
  constexpr auto kBufferSize = INET_ADDRSTRLEN;
  std::array<char, kBufferSize> str;

  auto sock_addr = ToSockAddr(addr, 0);

  const auto result =
      inet_ntop(AF_INET, &sock_addr.sin_addr, str.data(), kBufferSize);

  if (!result) {
    throw std::runtime_error{"Could not convert address"};
  }

  return std::string{str.data()};
}

template <> std::string ToString(const Ipv6 &addr) {
  constexpr auto kBufferSize = INET6_ADDRSTRLEN;
  std::array<char, kBufferSize> str;

  auto sock_addr = ToSockAddr(addr, 0);

  const auto result =
      inet_ntop(AF_INET6, &sock_addr.sin6_addr, str.data(), kBufferSize);

  if (!result) {
    throw std::runtime_error{"Could not convert address"};
  }

  return std::string{str.data()};
}

Addr::Addr() : version_{IpVersion::kTotal} {}

Addr::Addr(Ipv4 ipv4_addr)
    : ipv4{std::move(ipv4_addr)}, version_{IpVersion::kIpv4} {}

Addr::Addr(Ipv6 ipv6_addr)
    : ipv6{std::move(ipv6_addr)}, version_{IpVersion::kIpv6} {}

const Ipv4 &Addr::GetIpv4() const { return ipv4; }

const Ipv6 &Addr::GetIpv6() const { return ipv6; }

Addr ResolveAddr(const std::string &addr) {
  addrinfo *info;
  if (getaddrinfo(addr.c_str(), nullptr, nullptr, &info)) {
    throw AddrResolveException{addr};
  }

  if (!info) {
    throw AddrResolveException{addr};
  }

  const auto result = [info, &addr] {
    for (auto *it = info; it != nullptr; it = it->ai_next) {
      if (it->ai_family == AF_INET || it->ai_family == AF_INET6) {
        return ParseAddr(*it);
      }
    }
    throw AddrResolveException{addr, "No suitable address"};
  }();

  freeaddrinfo(info);

  return result;
}

AddrResolveException::AddrResolveException(const std::string &addr)
    : std::runtime_error{"Could not resolve hostname " + addr} {}

AddrResolveException::AddrResolveException(const std::string &addr,
                                           const std::string &why)
    : std::runtime_error{"Could not resolve hostname " + addr + ": " + why} {}

} // namespace addr
} // namespace core
} // namespace udpshowcase
