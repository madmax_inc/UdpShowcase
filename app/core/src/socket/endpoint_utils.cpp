#include "socket/endpoint_utils.hpp"

namespace udpshowcase {
namespace core {

namespace {

template <class InAddrType, class AddrType>
const InAddrType *addr_cast(const AddrType &addr) {
  static_assert(sizeof(addr.data) == sizeof(InAddrType));
  return reinterpret_cast<const InAddrType *>(addr.data.data());
}

template <class AddrType, class InAddrType>
AddrType addr_cast_in(const InAddrType &in_addr) {
  static_assert(sizeof(in_addr) == sizeof(AddrType));
  AddrType addr{};
  std::copy(reinterpret_cast<const common::byte_t *>(&in_addr),
            reinterpret_cast<const common::byte_t *>(&in_addr) +
                sizeof(in_addr),
            addr.data.begin());
  return addr;
}

} // namespace

sockaddr_in ToSockAddr(const addr::Ipv4 &ip_addr, Port port) {
  sockaddr_in addr;
  addr.sin_family = AF_INET;
  addr.sin_addr.s_addr = *addr_cast<in_addr_t>(ip_addr);
  addr.sin_port = htons(port);
  return addr;
}

sockaddr_in6 ToSockAddr(const addr::Ipv6 &ip_addr, Port port) {
  sockaddr_in6 addr;
  addr.sin6_family = AF_INET6;
  addr.sin6_addr = *addr_cast<in6_addr>(ip_addr);
  addr.sin6_port = htons(port);
  return addr;
}

Endpoint FromSockAddr(const sockaddr_in &addr) {
  return {addr_cast_in<addr::Ipv4>(addr.sin_addr), ntohs(addr.sin_port)};
}
Endpoint FromSockAddr(const sockaddr_in6 &addr) {
  return {addr_cast_in<addr::Ipv6>(addr.sin6_addr), ntohs(addr.sin6_port)};
}

} // namespace core
} // namespace udpshowcase
