#include "socket/socket.hpp"

#include <functional>

#include <fcntl.h>
#include <netinet/ip.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "socket/endpoint_utils.hpp"
#include "socket/exceptions.hpp"

namespace udpshowcase {
namespace core {

namespace {

bool WouldBlock(exception::SocketErrorCode error_code) {
  return error_code == EWOULDBLOCK || error_code == EAGAIN;
}

bool WouldBlock() { return WouldBlock(exception::SocketErrorCode{errno}); }

int GetAddrType(const Endpoint &endpoint) {
  switch (endpoint.addr.GetVersion()) {
  case addr::IpVersion::kIpv4:
    return AF_INET;
  case addr::IpVersion::kIpv6:
    return AF_INET6;
  default:
    throw std::runtime_error{"Unexpected ip version"};
  };
}

template <class R>
R MakeForAddr(const Endpoint &endpoint,
              std::function<R(const sockaddr *, socklen_t)> action) {
  switch (endpoint.addr.GetVersion()) {
  case addr::IpVersion::kIpv4: {
    const auto sock_addr = ToSockAddr(endpoint.addr.GetIpv4(), endpoint.port);
    return action(reinterpret_cast<const sockaddr *>(&sock_addr),
                  sizeof(sock_addr));
  }
  case addr::IpVersion::kIpv6: {
    const auto sock_addr = ToSockAddr(endpoint.addr.GetIpv6(), endpoint.port);
    return action(reinterpret_cast<const sockaddr *>(&sock_addr),
                  sizeof(sock_addr));
  }
  default:
    throw std::runtime_error{"Unexpected ip version"};
  }
}

SocketDescriptor MakeSocketDescriptor(const Endpoint &endpoint) {
  const SocketDescriptor descriptor =
      socket(GetAddrType(endpoint), SOCK_DGRAM, IPPROTO_UDP);

  if (descriptor == -1) {
    throw exception::SocketCreateException{
        exception::SocketErrorCode{descriptor}};
  }

  return descriptor;
}

Endpoint ParseEndpoint(const sockaddr_storage &sockaddr) {
  switch (sockaddr.ss_family) {
  case AF_INET: {
    const auto *addr = reinterpret_cast<const sockaddr_in *>(&sockaddr);
    return FromSockAddr(*addr);
  }
  case AF_INET6: {
    const auto *addr = reinterpret_cast<const sockaddr_in6 *>(&sockaddr);
    return FromSockAddr(*addr);
  }
  default:
    throw std::runtime_error{""};
  }
}

std::size_t WriteImpl(const SocketDescriptor descriptor,
                      const common::ByteString &data, const sockaddr *sock_addr,
                      socklen_t sock_addr_len) {
  iovec iov[1];
  iov[0].iov_base = const_cast<common::byte_t *>(data.data());
  iov[0].iov_len = data.length();

  msghdr message{};
  message.msg_name = const_cast<sockaddr *>(sock_addr);
  message.msg_namelen = sock_addr_len;
  message.msg_iov = iov;
  message.msg_iovlen = 1;
  message.msg_control = nullptr;
  message.msg_controllen = 0;

  const auto write_result = sendmsg(descriptor, &message, 0);

  if (write_result == -1) {
    if (WouldBlock()) {
      throw exception::SocketWouldBlockException{};
    }

    throw exception::SocketWriteException{errno};
  }

  return static_cast<std::size_t>(write_result);
}

} // namespace

SocketHolder::SocketHolder(SocketDescriptor descriptor)
    : socket_descriptor_{descriptor} {
  const auto flags = fcntl(descriptor, F_GETFL);
  fcntl(descriptor, F_SETFL, flags | O_NONBLOCK);
}

SocketHolder::SocketHolder(SocketHolder &&rhs)
    : socket_descriptor_{rhs.socket_descriptor_} {
  rhs.socket_descriptor_ = SocketDescriptor{-1};
}

SocketHolder::~SocketHolder() {
  if (socket_descriptor_ > 0) {
    close(socket_descriptor_);
  }
}

Data UdpSocketHolder::Read(std::size_t max) const {
  common::ByteString buffer;
  buffer.resize(max);

  iovec iov[1];
  iov[0].iov_base = const_cast<common::byte_t *>(buffer.data());
  iov[0].iov_len = max;

  sockaddr_storage src_addr;

  msghdr message{};
  message.msg_name = &src_addr;
  message.msg_namelen = sizeof(src_addr);
  message.msg_iov = iov;
  message.msg_iovlen = 1;
  message.msg_control = nullptr;
  message.msg_controllen = 0;

  const auto count = recvmsg(this->socket_descriptor_, &message, 0);

  if (count == -1) {
    if (WouldBlock()) {
      throw exception::SocketWouldBlockException{};
    }

    throw exception::SocketReadException{errno};
  }

  if (message.msg_flags & MSG_TRUNC) {
    throw exception::SocketReadMessageTruncated{};
  }

  buffer.resize(count);

  return {/*.buffer = */ std::move(buffer),
          /*.endpoint = */ ParseEndpoint(src_addr)};
}

UdpServerSocket::UdpServerSocket(const Endpoint &endpoint)
    : UdpSocketHolder{MakeSocketDescriptor(endpoint)} {
  const int bind_result = MakeForAddr<int>(
      endpoint, std::bind(&bind, this->socket_descriptor_,
                          std::placeholders::_1, std::placeholders::_2));
  if (bind_result == -1) {
    throw exception::SocketBindException{errno};
  }
}

std::size_t UdpServerSocket::Write(const Data &data) const {
  const auto bytes_written = MakeForAddr<std::size_t>(
      data.endpoint,
      std::bind(&WriteImpl, this->socket_descriptor_, data.buffer,
                std::placeholders::_1, std::placeholders::_2));

  return bytes_written;
}

UdpClientSocket::UdpClientSocket(const Endpoint &endpoint)
    : UdpSocketHolder{MakeSocketDescriptor(endpoint)} {
  const int connect_result = MakeForAddr<int>(
      endpoint, std::bind(&connect, this->socket_descriptor_,
                          std::placeholders::_1, std::placeholders::_2));

  if (connect_result == -1) {
    if (!WouldBlock()) {
      throw exception::SocketConnectException{errno};
    }
  }
}

std::size_t UdpClientSocket::Write(const common::ByteString &data) const {
  const auto bytes_written =
      WriteImpl(this->socket_descriptor_, data, nullptr, 0);

  return bytes_written;
}

} // namespace core
} // namespace udpshowcase
