#pragma once

#include <socket/endpoint.hpp>

#include <netinet/in.h>

namespace udpshowcase {
namespace core {

sockaddr_in ToSockAddr(const addr::Ipv4 &ip_addr, Port port);
sockaddr_in6 ToSockAddr(const addr::Ipv6 &ip_addr, Port port);

Endpoint FromSockAddr(const sockaddr_in &addr);
Endpoint FromSockAddr(const sockaddr_in6 &addr);

} // namespace core
} // namespace udpshowcase
