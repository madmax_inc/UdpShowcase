#include "socket/endpoint.hpp"

namespace udpshowcase {
namespace core {

namespace {

constexpr auto kIpv4 = "Ipv4";
constexpr auto kIpv6 = "Ipv6";

struct IpInfo {
  const char *ip_type;
  std::string ip;
  Port port;
};

IpInfo ToIpInfo(const Endpoint &endpoint) {
  switch (endpoint.addr.GetVersion()) {
  case addr::IpVersion::kIpv4: {
    const auto &ip = endpoint.addr.GetIpv4();
    return {
        kIpv4,
        ToString(ip),
        endpoint.port,
    };
  }
  case addr::IpVersion::kIpv6: {
    const auto &ip = endpoint.addr.GetIpv6();
    return {
        kIpv6,
        ToString(ip),
        endpoint.port,
    };
  }
  default:
    throw std::runtime_error{"Unexpected Ip version"};
  }
}

} // namespace

std::ostream &operator<<(std::ostream &stream, const Endpoint &endpoint) {
  const auto ip_info = ToIpInfo(endpoint);

  stream << "InetEndpoint<" << ip_info.ip_type << "> { .ip=" << ip_info.ip
         << ", .port=" << ip_info.port << "}";
  return stream;
}

} // namespace core
} // namespace udpshowcase
