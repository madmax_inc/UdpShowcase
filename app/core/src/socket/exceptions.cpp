#include "socket/exceptions.hpp"

#include <cstring>

namespace udpshowcase {
namespace exception {

namespace {

std::string ErrorString(SocketErrorCode code) { return strerror(code); }

} // namespace

SocketCreateException::SocketCreateException(SocketErrorCode err)
    : SocketCreateException{ErrorString(err)} {}
SocketCreateException::SocketCreateException(const std::string &why)
    : SocketInitializationException{"Error while creating socket: " + why} {}
SocketBindException::SocketBindException(SocketErrorCode err)
    : SocketInitializationException{"Error while binding socket: " +
                                    ErrorString(err)} {}
SocketConnectException::SocketConnectException(SocketErrorCode err)
    : SocketInitializationException{"Error while connecting: " +
                                    ErrorString(err)} {}

SocketReadException::SocketReadException(SocketErrorCode err)
    : SocketReadException{ErrorString(err)} {}

SocketReadException::SocketReadException(const std::string &why)
    : SocketIOException{"Error while reading: " + why} {}

SocketReadMessageTruncated::SocketReadMessageTruncated()
    : SocketReadException{"Error while reading: message truncated"} {}
SocketWriteException::SocketWriteException(SocketErrorCode err)
    : SocketIOException{"Error while writing: " + ErrorString(err)} {}

SocketTimeoutExpiredException::SocketTimeoutExpiredException(
    const std::string &what)
    : SocketIOException{"Timeout expired while waiting for socket: " + what} {}

SocketWouldBlockException::SocketWouldBlockException()
    : SocketIOException{"Socket IO failed: would block"} {}

} // namespace exception
} // namespace udpshowcase
