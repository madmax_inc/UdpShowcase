project(UdpShowcaseCore CXX)
cmake_minimum_required(VERSION 3.0)

SET(CMAKE_CXX_STANDARD 11)

set(SOURCES
        include/dispatcher.hpp
        src/dispatcher.cpp

        include/timeout.hpp

        include/addr.hpp
        src/addr.cpp

	include/socket/socket.hpp
        src/socket/socket.cpp

        include/socket/exceptions.hpp
        src/socket/exceptions.cpp

        include/socket/endpoint.hpp
        src/socket/endpoint.cpp
        src/socket/endpoint_utils.hpp
        src/socket/endpoint_utils.cpp)

include_directories(include src)

add_library(UdpShowcaseCore STATIC ${SOURCES})
target_link_libraries(UdpShowcaseCore UdpShowcaseCommon)
target_include_directories(UdpShowcaseCore PUBLIC include)
