#pragma once

#include <chrono>

#include <utils.hpp>

namespace udpshowcase {
namespace core {
namespace timeout {

constexpr std::chrono::milliseconds kNoTimeout{-1};

}
} // namespace core
} // namespace udpshowcase
