#pragma once

#include <chrono>
#include <functional>
#include <vector>

#include <expected.hpp>
#include <socket/socket.hpp>
#include <timeout.hpp>

namespace udpshowcase {
namespace core {

class Dispatcher {
public:
  using GeneralCallback = std::function<void(common::Expected<void>)>;

  template <class Socket, class ReadCallback>
  void
  AsyncRead(const Socket &socket, std::size_t max, ReadCallback read_callback,
            const std::chrono::milliseconds &timeout = timeout::kNoTimeout) {
    NotifyReadyRead(socket,
                    [&socket, max, read_callback,
                     timeout](const common::Expected<void> &good) {
                      if (!good) {
                        read_callback(common::Expected<Data>{good.Bad()});
                        return;
                      }

                      try {
                        read_callback(socket.Read(max));
                      } catch (const exception::SocketIOException &) {
                        read_callback(
                            common::Expected<Data>{std::current_exception()});
                      }
                    },
                    timeout);
  }

  template <class Socket, class WriteData, class WriteCallback>
  void
  AsyncWrite(const Socket &socket, const WriteData &data,
             WriteCallback write_callback,
             const std::chrono::milliseconds &timeout = timeout::kNoTimeout) {
    NotifyReadyWrite(
        socket,
        [&socket, data, write_callback](const common::Expected<void> &good) {
          if (!good) {
            write_callback(common::Expected<std::size_t>{good.Bad()});
          }

          try {
            write_callback(common::Expected<std::size_t>{socket.Write(data)});
          } catch (const exception::SocketIOException &) {
            write_callback(
                common::Expected<std::size_t>{std::current_exception()});
          }
        },
        timeout);
  }

  void NotifyReadyRead(
      const SocketHolder &socket, GeneralCallback read_callback,
      const std::chrono::milliseconds &timeout = timeout::kNoTimeout);
  void NotifyReadyWrite(
      const SocketHolder &socket, GeneralCallback write_callback,
      const std::chrono::milliseconds &timeout = timeout::kNoTimeout);

  [[noreturn]] void Run();
  void Dispatch(const std::chrono::milliseconds &timeout = timeout::kNoTimeout);

private:
  void DispatchQueues();

  struct DefferedInfo {
    std::reference_wrapper<const SocketHolder> holder;
    GeneralCallback callback;
    std::chrono::milliseconds timeout;
  };

  common::DefferedVector<DefferedInfo> read_queue_;
  common::DefferedVector<DefferedInfo> write_queue_;
};

template <class Socket> class AsyncSocket {
public:
  AsyncSocket(const Socket &socket, Dispatcher &dispatcher)
      : socket_{socket}, dispatcher_{dispatcher} {}

  template <class ReadCallback>
  void Read(std::size_t max, ReadCallback callback) {
    dispatcher_.AsyncRead(socket_, max, callback);
  }

  template <class WriteData, class WriteCallback>
  void Write(const WriteData &data, WriteCallback callback) {
    dispatcher_.AsyncWrite(socket_, data, callback);
  }

private:
  const Socket &socket_;
  Dispatcher &dispatcher_;
};

} // namespace core
} // namespace udpshowcase
