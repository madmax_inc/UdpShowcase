#pragma once

#include <chrono>
#include <optional>

#include <addr.hpp>
#include <socket/endpoint.hpp>
#include <socket/exceptions.hpp>
#include <utils.hpp>

namespace udpshowcase {
namespace core {

struct SocketDescriptor : common::StrongTypedef<int> {
  using StrongTypedef<int>::StrongTypedef;
};

/**
 * @brief The SocketHolder class
 * A basic RAII system socket guard
 */
class SocketHolder {
public:
  SocketHolder(const SocketHolder &) = delete;
  SocketHolder(SocketHolder &&rhs);

  SocketDescriptor GetSocketDescriptor() const { return socket_descriptor_; }

protected:
  SocketHolder(SocketDescriptor);
  ~SocketHolder();

  SocketDescriptor socket_descriptor_;
};

struct Data {
  common::ByteString buffer;
  Endpoint endpoint;
};

class UdpSocketHolder : public SocketHolder {
  using SocketHolder::SocketHolder;

public:
  Data Read(std::size_t max) const;
};

class UdpServerSocket : public UdpSocketHolder {
public:
  UdpServerSocket(const Endpoint &endpoint);

  std::size_t Write(const Data &data) const;
};

class UdpClientSocket : public UdpSocketHolder {
public:
  UdpClientSocket(const Endpoint &endpoint);

  std::size_t Write(const common::ByteString &data) const;
};

} // namespace core
} // namespace udpshowcase
