#pragma once

#include <stdexcept>

#include <utils.hpp>

namespace udpshowcase {
namespace exception {

class SocketErrorCode : public common::StrongTypedef<int> {
  using StrongTypedef<int>::StrongTypedef;
};

class SocketException : public std::runtime_error {
  using std::runtime_error::runtime_error;

public:
  SocketException(SocketErrorCode);
};

class SocketInitializationException : public SocketException {
  using SocketException::SocketException;
};

class SocketCreateException : public SocketInitializationException {
public:
  explicit SocketCreateException(SocketErrorCode);
  explicit SocketCreateException(const std::string &why);
};

class SocketBindException : public SocketInitializationException {
public:
  explicit SocketBindException(SocketErrorCode);
};

class SocketConnectException : public SocketInitializationException {
public:
  explicit SocketConnectException(SocketErrorCode);
};

class SocketIOException : public SocketException {
  using SocketException::SocketException;
};

class SocketWouldBlockException : public SocketIOException {
public:
  explicit SocketWouldBlockException();
};

class SocketReadException : public SocketIOException {
public:
  explicit SocketReadException(SocketErrorCode);
  explicit SocketReadException(const std::string &why);
};

class SocketReadMessageTruncated : public SocketReadException {
public:
  explicit SocketReadMessageTruncated();
};

class SocketWriteException : public SocketIOException {
public:
  explicit SocketWriteException(SocketErrorCode);
};

class SocketTimeoutExpiredException : public SocketIOException {
public:
  explicit SocketTimeoutExpiredException(const std::string &what);
};

} // namespace exception
} // namespace udpshowcase
