#pragma once

#include <cstdint>
#include <ostream>

#include "addr.hpp"

namespace udpshowcase {
namespace core {

using Port = std::uint16_t;

struct Endpoint {
  addr::Addr addr;
  Port port;
};

std::ostream &operator<<(std::ostream &stream, const Endpoint &endpoint);

} // namespace core
} // namespace udpshowcase
