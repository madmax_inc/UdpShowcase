#pragma once

#include <array>
#include <ostream>
#include <string>

#include <utils.hpp>

namespace udpshowcase {
namespace core {
namespace addr {

enum class IpVersion {
  kIpv4,
  kIpv6,

  kTotal,
};

template <std::size_t size> struct Ip {
  std::array<common::byte_t, size> data;

  static constexpr std::size_t Len() { return size; }

  bool operator==(const Ip<size> &rhs) const { return data == rhs.data; }
};

using Ipv4 = Ip<4>;
using Ipv6 = Ip<16>;

template <std::size_t size> static Ip<size> any() { return {{0x00}}; };

template <class Addr> static Addr any() { return any<Addr::Len()>(); }

template <std::size_t size> std::string ToString(const Ip<size> &addr);

class Addr {
public:
  Addr();
  Addr(Ipv4);
  Addr(Ipv6);

  IpVersion GetVersion() const { return version_; }

  const Ipv4 &GetIpv4() const;
  const Ipv6 &GetIpv6() const;

private:
  union {
    Ipv4 ipv4;
    Ipv6 ipv6;
  };

  IpVersion version_;
};

class AddrResolveException : public std::runtime_error {
public:
  explicit AddrResolveException(const std::string &addr);
  AddrResolveException(const std::string &addr, const std::string &why);
};

Addr ResolveAddr(const std::string &str);

} // namespace addr
} // namespace core
} // namespace udpshowcase
