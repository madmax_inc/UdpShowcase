#pragma once

#include <socket/endpoint.hpp>

namespace udpshowcase {
namespace client {

class InvalidArgs : public std::runtime_error {
public:
  explicit InvalidArgs();
  std::string Usage() const;
};

struct Args {
  Args(int argc, char **argv);

  core::Endpoint endpoint;
  std::size_t packets_num{100};
};

} // namespace client
} // namespace udpshowcase
