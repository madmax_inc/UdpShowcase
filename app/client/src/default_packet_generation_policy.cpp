#include "default_packet_generation_policy.hpp"

#include <chrono>

namespace udpshowcase {
namespace client {

namespace {

unsigned int seed() {
  return static_cast<unsigned int>(
      std::chrono::system_clock::now().time_since_epoch().count());
}

} // namespace

DefaultPacketGenerationPolicy::DefaultPacketGenerationPolicy()
    : gen_{seed()}, dist_{std::numeric_limits<std::int32_t>::min(),
                          std::numeric_limits<std::int32_t>::max()} {}

} // namespace client
} // namespace udpshowcase
