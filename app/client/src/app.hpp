#pragma once

#include <iostream>

#include <dispatcher.hpp>
#include <expected.hpp>
#include <packet.hpp>
#include <socket/socket.hpp>

#include <csignal>

namespace udpshowcase {
namespace client {

template <class PacketGenerationPolicy> class App : PacketGenerationPolicy {
public:
  App(core::UdpClientSocket socket, std::size_t packets)
      : socket_{std::move(socket)}, packets_{packets} {
    DeferWrite();
  }

  void operator()() {
    while (counter_ < packets_ && running_) {
      dispatcher_.Dispatch(std::chrono::milliseconds{50});
    }
  }

private:
  void DeferWrite() {
    if (counter_ >= packets_) {
      return;
    }
    const auto packet = this->makePacket();

    std::cout << "Sending packet " << packet << std::endl;
    dispatcher_.AsyncWrite(
        socket_, common::TransmittablePacket{packet}.ToByteString(),
        [this](const common::Expected<std::size_t> &bytes_written) {
          if (!bytes_written) {
            HandleIOError(bytes_written.Bad());
            this->DeferWrite();
            return;
          }
          this->DeferRead();
        });
  }

  void DeferRead() {
    dispatcher_.AsyncRead(socket_, sizeof(common::Packet),
                          [this](const common::Expected<core::Data> &result) {
                            if (!result) {
                              HandleIOError(result.Bad());
                            } else {
                              std::cout
                                  << "Recieved reply "
                                  << common::TransmittablePacket{result->buffer}
                                  << std::endl;
                            }

                            counter_++;
                            this->DeferWrite();
                          },
                          std::chrono::milliseconds{500});
  }

  static void HandleIOError(std::exception_ptr exception) {
    try {
      if (exception) {
        std::rethrow_exception(exception);
      }
    } catch (const exception::SocketIOException &e) {
      std::cerr << e.what() << std::endl;
    }
  }

  core::UdpClientSocket socket_;
  core::Dispatcher dispatcher_;
  const std::size_t packets_;
  std::size_t counter_{};
  static volatile bool running_;
};

template <class PacketGenerationPolicy>
volatile bool App<PacketGenerationPolicy>::running_ = [] {
  signal(SIGINT, [](int) { running_ = false; });
  return true;
}();

} // namespace client
} // namespace udpshowcase
