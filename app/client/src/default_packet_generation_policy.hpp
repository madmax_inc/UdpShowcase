#pragma once

#include <packet.hpp>
#include <random>

namespace udpshowcase {
namespace client {

class DefaultPacketGenerationPolicy {
public:
  explicit DefaultPacketGenerationPolicy();
  common::Packet makePacket() { return {packet_no_++, dist_(gen_)}; }

private:
  std::uint32_t packet_no_{};
  std::default_random_engine gen_;
  std::uniform_int_distribution<std::int32_t> dist_;
};

} // namespace client
} // namespace udpshowcase
