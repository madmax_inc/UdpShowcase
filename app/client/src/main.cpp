#include <iostream>

#include <socket/socket.hpp>

#include "app.hpp"
#include "args.hpp"
#include "default_packet_generation_policy.hpp"

using namespace udpshowcase;

int main(int argc, char **argv) {
  enum {
    kInvalidArgs = 1,
    kInitializationError,
    kAddressNotResolved,
  };

  const auto args = [argc, argv] {
    try {
      return client::Args{argc, argv};
    } catch (const client::InvalidArgs &e) {
      std::cout << e.Usage() << std::endl;
      std::exit(kInvalidArgs);
    } catch (const core::addr::AddrResolveException &e) {
      std::cerr << e.what() << std::endl;
      std::exit(kAddressNotResolved);
    }
  }();

  auto socket = [&args] {
    try {
      return core::UdpClientSocket{args.endpoint};
    } catch (const exception::SocketInitializationException &e) {
      std::cerr << e.what() << std::endl;
      std::exit(kInitializationError);
    }
  }();

  client::App<client::DefaultPacketGenerationPolicy>{std::move(socket),
                                                     args.packets_num}();
}
