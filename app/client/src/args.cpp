#include "args.hpp"

namespace udpshowcase::client {

InvalidArgs::InvalidArgs() : std::runtime_error{Usage()} {}

std::string InvalidArgs::Usage() const {
  return "Usage: UdpShowcaseClient <Host> <PortNo> [<Packets Num>]";
}

Args::Args(int argc, char **argv) {
  if (argc < 3) {
    throw InvalidArgs{};
  }

  const auto addr = core::addr::ResolveAddr(argv[1]);
  const auto port = [argv] {
    try {
      return core::Port(std::stoul(argv[2]));
    } catch (const std::invalid_argument &) {
      throw InvalidArgs{};
    }
  }();

  endpoint = {addr, port};

  if (argc == 4) {
    packets_num = [argv] {
      try {
        return std::stoul(argv[3]);
      } catch (const std::invalid_argument &) {
        throw InvalidArgs{};
      }
    }();
  }
}

} // namespace udpshowcase::client
