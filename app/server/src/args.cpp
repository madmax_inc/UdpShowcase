#include "args.hpp"

namespace udpshowcase {
namespace server {

InvalidArgs::InvalidArgs() : std::runtime_error{Usage()} {}

std::string InvalidArgs::Usage() const {
  return "Usage: UdpShowcaseServer <PortNo> [--ipv4/--ipv6]";
}

Args::Args(int argc, char **argv) {
  constexpr auto kIpv4 = "--ipv4";
  constexpr auto kIpv6 = "--ipv6";

  if (argc < 2) {
    throw InvalidArgs{};
  }

  const auto port = [&argv] {
    try {
      return core::Port(std::stoul(argv[1]));
    } catch (const std::invalid_argument &) {
      throw InvalidArgs{};
    }
  }();

  const auto mode = [argc, &argv] {
    if (argc == 2) {
      return core::addr::IpVersion::kIpv4;
    }

    const auto flag = std::string(argv[2]);

    if (flag == kIpv4) {
      return core::addr::IpVersion::kIpv4;
    } else if (flag == kIpv6) {
      return core::addr::IpVersion::kIpv6;
    } else {
      throw InvalidArgs{};
    }
  }();

  const auto addr = [mode]() -> core::addr::Addr {
    switch (mode) {
    case core::addr::IpVersion::kIpv4:
      return core::addr::any<core::addr::Ipv4>();
    case core::addr::IpVersion::kIpv6:
      return core::addr::any<core::addr::Ipv6>();
    default:
      throw std::runtime_error{"Unexpected ip version"};
    }
  }();

  endpoint = {addr, port};
}

} // namespace server
} // namespace udpshowcase
