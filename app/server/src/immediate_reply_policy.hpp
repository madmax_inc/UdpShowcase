#pragma once

#include <dispatcher.hpp>
#include <packet.hpp>
#include <socket/socket.hpp>

namespace udpshowcase {
namespace server {

class ImmediateReplyPolicy {
protected:
  explicit ImmediateReplyPolicy(
      core::AsyncSocket<core::UdpServerSocket> async_socket);

  void Init() { DeferRead(); }

private:
  void DeferRead();
  common::TransmittablePacket HandleData(const core::Data &data);
  void DeferWrite(const common::TransmittablePacket &packet,
                  const core::Endpoint &endpoint);

  core::AsyncSocket<core::UdpServerSocket> async_socket_;
  common::TransmittablePacket prepared_response_;
};

} // namespace server
} // namespace udpshowcase
