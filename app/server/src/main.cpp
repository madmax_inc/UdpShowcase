#include <iostream>

#include <socket/exceptions.hpp>
#include <socket/socket.hpp>

#include "app.hpp"
#include "args.hpp"
#include "immediate_reply_policy.hpp"

using namespace udpshowcase;
using namespace udpshowcase::core;
using namespace udpshowcase::server;

int main(int argc, char **argv) {
  constexpr auto kInvalidArgs = 1;
  constexpr auto kInitializationError = 2;

  const auto endpoint = [argc, argv] {
    try {
      return Args{argc, argv}.endpoint;
    } catch (const InvalidArgs &e) {
      std::cout << e.Usage() << std::endl;
      std::exit(kInvalidArgs);
    }
  }();

  auto socket = [&endpoint] {
    try {
      return UdpServerSocket{endpoint};
    } catch (const exception::SocketInitializationException &e) {
      std::cerr << e.what() << std::endl;
      std::exit(kInitializationError);
    }
  }();

  App<ImmediateReplyPolicy>{std::move(socket)}();
}
