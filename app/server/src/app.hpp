#pragma once

#include <csignal>

#include <dispatcher.hpp>
#include <packet.hpp>
#include <socket/socket.hpp>

namespace udpshowcase {
namespace server {

template <class ReplyPolicy> class App : ReplyPolicy {
public:
  explicit App(core::UdpServerSocket socket)
      : ReplyPolicy{core::AsyncSocket<core::UdpServerSocket>{socket_,
                                                             dispatcher_}},
        socket_{std::move(socket)} {
    this->Init();
  }

  void operator()() {
    while (running_) {
      dispatcher_.Dispatch();
    }
  }

private:
  core::UdpServerSocket socket_;
  core::Dispatcher dispatcher_;
  static volatile bool running_;
};

template <class ReplyPolicy>
volatile bool App<ReplyPolicy>::running_ = [] {
  signal(SIGINT, [](int) { running_ = false; });
  return true;
}();

} // namespace server
} // namespace udpshowcase
