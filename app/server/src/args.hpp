#pragma once

#include <stdexcept>

#include <socket/endpoint.hpp>

namespace udpshowcase {
namespace server {

class InvalidArgs : public std::runtime_error {
public:
  explicit InvalidArgs();
  std::string Usage() const;
};

struct Args {
  Args(int argc, char **argv);

  core::Endpoint endpoint;
};

} // namespace server
} // namespace udpshowcase
