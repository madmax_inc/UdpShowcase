#include "immediate_reply_policy.hpp"

#include <iostream>

#include <packet.hpp>

namespace udpshowcase {
namespace server {

namespace {

void HandleIOError(std::exception_ptr exception) {
  try {
    if (exception) {
      std::rethrow_exception(exception);
    }
  } catch (const exception::SocketIOException &e) {
    std::cerr << e.what() << std::endl;
  }
}

} // namespace

ImmediateReplyPolicy::ImmediateReplyPolicy(
    core::AsyncSocket<core::UdpServerSocket> async_socket)
    : async_socket_{async_socket} {}

void ImmediateReplyPolicy::DeferRead() {
  async_socket_.Read(sizeof(common::Packet),
                     [this](const common::Expected<core::Data> &data) {
                       if (!data) {
                         HandleIOError(data.Bad());
                         return;
                       }

                       const auto reply = this->HandleData(*data);
                       this->DeferRead();
                       this->DeferWrite(reply, data->endpoint);
                     });
}

void ImmediateReplyPolicy::DeferWrite(const common::TransmittablePacket &data,
                                      const core::Endpoint &endpoint) {
  async_socket_.Write(core::Data{data.ToByteString(), endpoint},
                      [](const common::Expected<std::size_t> &bytes_written) {
                        if (!bytes_written) {
                          HandleIOError(bytes_written.Bad());
                        }
                      });
}

common::TransmittablePacket
ImmediateReplyPolicy::HandleData(const core::Data &data) {
  const common::TransmittablePacket packet{data.buffer};
  std::cout << "Recieved packet " << packet << " from " << data.endpoint
            << std::endl;

  const auto reply = prepared_response_;
  if (!prepared_response_) {
    prepared_response_ = packet;
  } else {
    prepared_response_->packet_no = packet->packet_no;
    prepared_response_->n = std::min(prepared_response_->n, packet->n);
  }

  return reply;
}

} // namespace server
} // namespace udpshowcase
