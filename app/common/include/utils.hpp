#pragma once

#include <cstdint>
#include <ostream>
#include <string>
#include <vector>

namespace udpshowcase {
namespace common {

using byte_t = std::uint8_t;

class ByteString : public std::basic_string<byte_t> {
  using std::basic_string<byte_t>::basic_string;

public:
  ByteString(std::basic_string<byte_t> &&str) : basic_string{std::move(str)} {}
};

std::ostream &operator<<(std::ostream &lhs, const ByteString &rhs);

template <class T> struct ClassTag {};

template <class T> class StrongTypedef {
public:
  template <class... Args>
  explicit StrongTypedef(Args &&... args)
      : impl_{std::forward<Args>(args)...} {}
  StrongTypedef(T rhs) : impl_{std::move(rhs)} {}

  StrongTypedef<T> &operator=(const StrongTypedef<T> &rhs) {
    impl_ = rhs;
    return *this;
  }

  StrongTypedef<T> &operator=(T rhs) { return (*this) = StrongTypedef{rhs}; }

  const T *operator->() const { return &impl_; }

  T *operator->() { return &impl_; }

  operator const T &() const { return impl_; }

  operator T &() { return impl_; }

private:
  T impl_;
};

template <class T> class DefferedVector {
public:
  std::vector<T> &Impl() { return impl_; }

  void DefferedPush(T &&value) { deffered_.push_back(value); }

  template <class U> void DefferedPush(U &&value) {
    deffered_.push_back(std::forward<U>(value));
  }

  void Dispatch() {
    std::copy(begin(deffered_), end(deffered_), std::back_inserter(impl_));
    deffered_.clear();
  }

private:
  std::vector<T> impl_;
  std::vector<T> deffered_;
};

} // namespace common
} // namespace udpshowcase
