#pragma once

#include <stdexcept>

namespace udpshowcase {
namespace common {

template <class E> class ThrowingPolicy {
protected:
  void Throw(const E &e) const { throw e; }
};

template <> class ThrowingPolicy<std::exception_ptr> {
protected:
  void Throw(std::exception_ptr error) const { std::rethrow_exception(error); }
};

template <class T, class E = std::exception_ptr>
class Expected : private ThrowingPolicy<E> {
public:
  Expected(T good) : good_{std::move(good)}, is_good_{true} {}
  Expected(E bad) : bad_{std::move(bad)}, is_good_{false} {}
  Expected(Expected<T, E> &&rhs) {
    if (IsGood()) {
      new (&good_) T{std::move(rhs.good_)};
    } else {
      new (&bad_) E{std::move(rhs.bad_)};
    }
  }
  ~Expected() {
    if (IsGood()) {
      good_.~T();
    } else {
      bad_.~E();
    }
  }

  bool IsGood() const { return is_good_; }
  bool IsBad() const { return !IsGood(); }

  const T &Good() const { return good_; }
  const E &Bad() const { return bad_; }

  const T *operator->() const {
    ThrowIfBad();

    return &Good();
  }

  operator bool() const { return IsGood(); }

  const T &operator*() const {
    ThrowIfBad();

    return Good();
  }

private:
  void ThrowIfBad() const {
    if (IsBad()) {
      this->Throw(Bad());
    }
  }

  union {
    T good_;
    E bad_;
  };

  bool is_good_{};
};

struct Dummy {};

template <class E> class Expected<void, E> : public Expected<Dummy, E> {
  using Expected<Dummy, E>::Expected;

public:
  Expected() : Expected<Dummy, E>{Dummy{}} {}
};

} // namespace common
} // namespace udpshowcase
