#pragma once

#include <cstdint>
#include <ostream>

#include <utils.hpp>

namespace udpshowcase {
namespace common {

struct Packet {
  std::uint32_t packet_no;
  std::int32_t n;
};

class TransmittablePacket {
public:
  TransmittablePacket(const ByteString &byte_string);
  TransmittablePacket(Packet packet);
  explicit TransmittablePacket();

  const Packet &Get() const;

  const Packet &operator*() const { return packet_; }

  Packet *operator->() { return &packet_; }

  const Packet *operator->() const { return &packet_; }

  operator bool() const { return !is_dummy_; }

  ByteString ToByteString() const;

private:
  Packet packet_;
  bool is_dummy_{};
};

std::ostream &operator<<(std::ostream &str, const TransmittablePacket &packet);

} // namespace common
} // namespace udpshowcase
