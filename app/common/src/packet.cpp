#include "packet.hpp"

#include <type_traits>

namespace udpshowcase {
namespace common {

namespace {

const bool isLittleEndian = [] {
  const std::uint32_t num = 1;
  return (*reinterpret_cast<const std::uint8_t *>(&num) == 1);
}();

ByteString ToLittleEndian(ByteString str) {
  if (isLittleEndian) {
    return str;
  } else {
    ByteString inverted(str.length(), 0x00);
    std::move(str.rbegin(), str.rend(), inverted.begin());
    return str;
  }
}

template <class T> ByteString ToLittleEndianByteString(T value) {
  ByteString native{reinterpret_cast<const ByteString::value_type *>(&value),
                    sizeof(value)};

  return ToLittleEndian(native);
}

template <class T> T ToValue(const ByteString &little_endian_encoded) {
  const auto native = ToLittleEndian(little_endian_encoded);

  return T{*reinterpret_cast<const T *>(native.data())};
}

} // namespace

TransmittablePacket::TransmittablePacket(const ByteString &byte_string)
    : is_dummy_{byte_string.length() == 1 && byte_string[0] == 0xFF} {
  if (is_dummy_) {
    return;
  }

  packet_ = {/*.packet_no = */ ToValue<std::uint32_t>(
                 byte_string.substr(0, sizeof(std::uint32_t))),
             /*.n = */ ToValue<std::int32_t>(
                 byte_string.substr(sizeof(std::uint32_t)))};
}

TransmittablePacket::TransmittablePacket(Packet packet)
    : packet_{std::move(packet)}, is_dummy_{false} {}

TransmittablePacket::TransmittablePacket() : is_dummy_{true} {}

ByteString TransmittablePacket::ToByteString() const {
  if (!(*this)) {
    return ByteString({0xFF});
  }

  return ToLittleEndianByteString((*this)->packet_no) +
         ToLittleEndianByteString((*this)->n);
}

std::ostream &operator<<(std::ostream &str, const TransmittablePacket &packet) {
  str << "Packet { ";

  if (!packet) {
    str << "empty }";
    return str;
  }

  str << ".packet_no=" << packet->packet_no << ", .n=" << packet->n << "}";
  return str;
}

} // namespace common
} // namespace udpshowcase
