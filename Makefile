clang-format:
	clang-format-7 -i $$(find app -name "*.*pp")

docker-example:
	cd Docker && docker-compose up --build
