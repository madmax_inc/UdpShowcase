# UDP Showcase

An example app for Netris.

## Task
Build a simple client + server apps:
Server should listen on a provided UDP port for a packets described below and respond with a similar packet:
 * .packet_no = last recieved packet no
 * .n = minimal n among all the recieved packets
If it was the first packet the server should respond by a single byte 0xFF (denotes an empty response)
All the fields transmitted in a little endian.

Client should send to a provided endpoint (command line arguments) N packets (desribed below).
First packet has packet_no=0 and each packet should have a random n. Every packet should have next successive packet_no.

Packet structure:
```cpp
struct Packet {
    std::uint32_t packet_no;
    std::int32_t n;
};
```

## Inside

There is a non-blocking sockets implementation inspired by boost::asio.
All the project could be build by a command:
```bash
mkdir build
cd build && cmake -DCMAKE_BUILD_TYPE=Release .. && make
```

Then each app could be run:
```bash
./UdpShowcaseServer <Port>
./UdpShowcaseClient <Host> <Port> <Packets Count>
``` 

For a simplicity there is a Docker image provided with docker-compose.yml:
```bash
docker-compose up --build
```

## Makefile targets
Following makefile targets are available:
```bash
make clang-format - Executes in-place clang-format for the project files
make docker-example - Executes the showcase under docker-compose
```

## Roadmap
 * Integrate Rxcpp to avoid callback hell
 * Integrate callbacks for connection management into a Dispatcher
